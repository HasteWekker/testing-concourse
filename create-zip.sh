# bin/sh

set -ex

zip -r "testing-concourse.zip" . -x @.gitignore -x ".git/*"
